TEX=admin-terminal.tex
PDF=admin-terminal.pdf

$(PDF): $(TEX)
	pdflatex $(TEX)

view: $(PDF)
	pdfcube -b 0:0:0 $(PDF)

suma_lenta: suma_lenta.c
	gcc -o $@ $<