/*
  suma_lenta.c
  
  Sencillo programa que usa la función "sleep()" para sumar muy
  léntamente los 1.000 primeros elementos de la serie armónica
 */

#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
  int seconds_each_iter=1;
  int i, N=1000;
  float suma=0.;
  printf("Sumando léntamente %d términos de la serie armónica\n", N);
  for (i = 1; i <= N; ++i)
    {
      suma = suma + 1./i;
      printf("Iteración %d, suma parcial: %f\n", i, suma);
      sleep(seconds_each_iter);
    }
  printf("Resultado de la suma: %f\n", suma);
  return 0;
}
